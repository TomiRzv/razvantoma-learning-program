public class Main52to55 {
    public static void main(String[] args) {

        //52//
        /*Car myCar = new Car();
        Car anotherCar = new Car();

        myCar.accelerate();
        anotherCar.brake();
    }
}

class Car {

    public void accelerate() {

        System.out.println("You are going faster.");
    }

    public void brake() {

        System.out.println("You are going slower.");
    }
}*/

        //53//
        /*Car myCar = new Car();
        Car anotherCar = new Car();

        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.brake();
        myCar.accelerate();

        anotherCar.brake();
    }
}

class Car {

    private int speed = 0;

    public void accelerate() {
        speed++;
        System.out.printf("You are going %d kilometres per hour.%n", speed);
    }

    public void brake() {
        speed--;
        System.out.printf("You are going %d kilometres per hour.%n", speed);
    }
}
*/
        //54//
        /*Car myCar = new Car("Tim's car");
        Car anotherCar = new Car("The Batmobile");

        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.brake();
        myCar.accelerate();

        anotherCar.brake();
    }
}

class Car {

    private int speed = 0;
    private String name;

    public Car(String carName) {
        name = carName;
    }

    public void accelerate() {
        speed++;
        System.out.printf("%s is going %d kilometres per hour.%n", name, speed);
    }

    public void brake() {
        speed--;
        System.out.printf("%s is going %d kilometres per hour.%n", name, speed);
    }
}*/

    //55//
        Masina myCar = new Masina("Tim's car");
        Masina anotherCar = new Masina("The Batmobile");

        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.brake();
        myCar.accelerate();

        anotherCar.brake();
    }
}

class Masina {

    private int speed = 0;
    private String name;

    public Masina(String carName) {
        name = carName;
    }

    public void accelerate() {
        speed++;
        showSpeed();
    }

    public void brake() {
        speed--;
        showSpeed();
    }

    private void showSpeed() {
        System.out.printf("%s is going %d miles per hour.%n", name, speed);
    }
}