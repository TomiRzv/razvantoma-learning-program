public class Main126 {
    public static void main(String[] args) {

        String courseName = "Learn Java for Beginners Crash Course";

        int position;

        position = indexOfIgnoreCase(courseName, " j");
        System.out.println(position);
    }

    private static int indexOfIgnoreCase(String text, String searchText) {
        String textLowerCase = text.toLowerCase();
        String searchTextLowerCase = searchText.toLowerCase();
        return textLowerCase.indexOf(searchTextLowerCase);
    }
}
