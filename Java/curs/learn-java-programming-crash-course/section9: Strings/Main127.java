public class Main127 {
    public static void main(String[] args) {

        String courseName = "Learn Java for Beginners Crash Course";

        int position = -1;

        do {
            position = indexOfIgnoreCase(courseName, " c", position + 1);
            System.out.println(position);
        } while (position != -1);
    }

    private static int indexOfIgnoreCase(String text, String searchText, int fromIndex) {
        String textLowerCase = text.toLowerCase();
        String searchTextLowerCase = searchText.toLowerCase();
        return textLowerCase.indexOf(searchTextLowerCase, fromIndex);
    }
}
