public class MainPoly {

    public static void main(String[] args) {

        CarPoly car = new CarPoly("2022 Blue Ferrari 296 GTS");
        runRace(car);

        CarPoly ferrari = new GasPoweredCar("2022 Blue Ferrari 296 GTS",
                15.4, 6);
        runRace(ferrari);
    }

    public static void runRace(CarPoly car) {

        car.startEngine();
        car.drive();
    }
}