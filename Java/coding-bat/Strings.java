public class Strings {

    public static void main(String[] args) {

     /* System.out.println(helloName("Razvan"));
        System.out.println(makeOutWord("{{}}", "Cinematograf"));
        System.out.println(firstHalf("Exit"));
        System.out.println(nonStart("Hello", "There"));
        System.out.println(theEnd("Hello", true));
        System.out.println(endsLy("oddly"));
        System.out.println(middleThree("Candy"));
        System.out.println(lastChars("chars", ""));
        System.out.println(seeColor("redxx"));
        System.out.println(extraFront("Hello"));
        System.out.println(startWord("hippo", "xip"));
        System.out.println(makeAbba("What", "Up"));
        System.out.println(extraEnd("Hello"));
        System.out.println(withoutEnd("coding"));
        System.out.println(left2("Hello"));
        System.out.println(withouEnd2(""));
        System.out.println(nTwice("Chocolate", 1));
        System.out.println(hasBad("xbadxx"));
        System.out.println(conCat("abc", "cat"));
        System.out.println(frontAgain("edited"));
        System.out.println(without2("HelloHe"));
        System.out.println(withoutX("Hxix"));
        System.out.println(makeTags("i", "Yay"));
        System.out.println(firstTwo("Hello"));
        System.out.println(comboString("aaa", "b"));
        System.out.println(right2("java"));
        System.out.println(middleTwo("string"));
        System.out.println(twoChar("java", 2));
        System.out.println(atFirst("h"));
        System.out.println(lastTwo("cat"));
        System.out.println(minCat("Hello", "java"));
        System.out.println(deFront("away")); */
        System.out.println(withoutX2("Hxy"));

    }

    public static String helloName(String name) {
        /* Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".
        helloName("Bob") → "Hello Bob!"
        helloName("Alice") → "Hello Alice!"
        helloName("X") → "Hello X!"
        */
        return "Hello " + name + "!";
    }

    public static String makeOutWord(String out, String word) {
        /* Given an "out" string length 4, such as "<<>>", and a word, return a new string where the word is in the middle of the out string,
        e.g. "<<word>>". Note: use str.substring(i, j) to extract the String starting at index i and going up to but not including index j.
        makeOutWord("<<>>", "Yay") → "<<Yay>>"
        makeOutWord("<<>>", "WooHoo") → "<<WooHoo>>"
        makeOutWord("[[]]", "word") → "[[word]]"
        */
        return out.substring(0, 2) + word + out.substring(2, 4);
    }

    public static String firstHalf(String str) {
        /* Given a string of even length, return the first half. So the string "WooHoo" yields "Woo".
        firstHalf("WooHoo") → "Woo"
        firstHalf("HelloThere") → "Hello"
        firstHalf("abcdef") → "abc"
        */
        return str.substring(0, str.length() / 2);
    }

    public static String nonStart(String a, String b) {
        /* Given 2 strings, return their concatenation, except omit the first char of each. The strings will be at least length 1.
        nonStart("Hello", "There") → "ellohere"
        nonStart("java", "code") → "avaode"
        nonStart("shotl", "java") → "hotlava"
        */
        return a.substring(1).concat(b.substring(1));
    }

    public static String theEnd(String str, boolean front) {
        /* Given a string, return a string length 1 from its front, unless front is false,
        in which case return a string length 1 from its back. The string will be non-empty.
        theEnd("Hello", true) → "H"
        theEnd("Hello", false) → "o"
        theEnd("oh", true) → "o"
        */
        return front ? str.substring(0, 1) : str.substring(str.length() - 1);
    }

    public static boolean endsLy(String str) {
    /* Given a string, return true if it ends in "ly".
    endsLy("oddly") → true
    endsLy("y") → false
    endsLy("oddy") → false
    */
        return str.endsWith("ly");
    }

    public static String middleThree(String str) {
      /* Given a string of odd length, return the string length 3 from its middle,
      so "Candy" yields "and". The string length will be at least 3.
      middleThree("Candy") → "and"
      middleThree("and") → "and"
      middleThree("solving") → "lvi"
      */
        return str.substring(str.length() / 2 - 1, str.length() / 2 + 2);
    }

    public static String lastChars(String a, String b) {
        /*Given 2 strings, a and b, return a new string made of the first char of a and the last char of b,
        so "yo" and "java" yields "ya". If either string is length 0, use '@' for its missing char.
        lastChars("last", "chars") → "ls"
        lastChars("yo", "java") → "ya"
        lastChars("hi", "") → "h@"
        */
        String first, last;
        if (a.length() > 0)
            first = a.substring(0, 1);
        else
            first = "@";
        if (b.length() > 0)
            last = b.substring(b.length() - 1);
        else
            last = "@";

        return first.concat(last);
    }

    public static String seeColor(String str) {
        /*
        Given a string, if the string begins with "red" or "blue" return that color string, otherwise return the empty string.
        seeColor("redxx") → "red"
        seeColor("xxred") → ""
        seeColor("blueTimes") → "blue"
        */
        if (str.startsWith("red"))
            return "red";
        if (str.startsWith("blue"))
            return "blue";
        return "";
    }

    public static String extraFront(String str) {
    /* Given a string, return a new string made of 3 copies of the first 2 chars of the original string.
     The string may be any length. If there are fewer than 2 chars, use whatever is there.
    extraFront("Hello") → "HeHeHe"
    extraFront("ab") → "ababab"
    extraFront("H") → "HHH"
     */
        if (str.length() < 2)
            return str + str + str;
        else
            return str.substring(0, 2) + str.substring(0, 2) + str.substring(0, 2);
    }

    public static String startWord(String str, String word) {
    /* Given a string and a second "word" string, we'll say that the word matches the string if it appears at the front of the string,
     except its first char does not need to match exactly. On a match, return the front of the string, or otherwise return the empty string.
     So, so with the string "hippo" the word "hi" returns "hi" and "xip" returns "hip". The word will be at least length 1.
    startWord("hippo", "hi") → "hi"
    startWord("hippo", "xip") → "hip"
    startWord("hippo", "i") → "h"
     */
        if (str.length() >= word.length() && str.substring(1, word.length()).equals(word.substring(1)))
            return str.substring(0, word.length());
        return "";
    }

    public static String makeAbba(String a, String b) {
        /* Given two strings, a and b, return the result of putting them together in the order abba,
        e.g. "Hi" and "Bye" returns "HiByeByeHi".
        makeAbba("Hi", "Bye") → "HiByeByeHi"
        makeAbba("Yo", "Alice") → "YoAliceAliceYo"
        makeAbba("What", "Up") → "WhatUpUpWhat"
         */
        return a + b + b + a;
    }

    public static String extraEnd(String str) {
        /*Given a string, return a new string made of 3 copies of the last 2 chars of the original string. The string length will be at least 2.
        extraEnd("Hello") → "lololo"
        extraEnd("ab") → "ababab"
        extraEnd("Hi") → "HiHiHi"
        */
        return str.substring(str.length() - 2) + str.substring(str.length() - 2) + str.substring(str.length() - 2);
    }

    public static String withoutEnd(String str) {
    /* Given a string, return a version without the first and last char,
    so "Hello" yields "ell". The string length will be at least 2.
    withoutEnd("Hello") → "ell"
    withoutEnd("java") → "av"
    withoutEnd("coding") → "odin"
    */
        return str.substring(1, str.length() - 1);
    }

    public static String left2(String str) {
        /*Given a string, return a "rotated left 2" version where the first 2 chars are moved to the end. The string length will be at least 2.
        left2("Hello") → "lloHe"
        left2("java") → "vaja"
        left2("Hi") → "Hi"
        */
        return str.substring(2).concat(str.substring(0, 2));
    }

    public static String withouEnd2(String str) {
        /* Given a string, return a version without both the first and last char of the string. The string may be any length, including 0.
        withouEnd2("Hello") → "ell"
        withouEnd2("abc") → "b"
        withouEnd2("ab") → ""
         */
        if (str.length() <= 2) {
            return "";
        } else {
            return str.substring(1, str.length() - 1);
        }
    }

    public static String nTwice(String str, int n) {
    /*Given a string and an int n, return a string made of the first and last n chars from the string. The string length will be at least n.
    nTwice("Hello", 2) → "Helo"
    nTwice("Chocolate", 3) → "Choate"
    nTwice("Chocolate", 1) → "Ce"
    */
        return str.substring(0, n) + str.substring(str.length() - n);
    }

    public static boolean hasBad(String str) {
        /* Given a string, return true if "bad" appears starting at index 0 or 1 in the string,
        such as with "badxxx" or "xbadxx" but not "xxbadxx". The string may be any length, including 0.
        Note: use .equals() to compare 2 strings.
        hasBad("badxx") → true
        hasBad("xbadxx") → true
        hasBad("xxbadxx") → false
        */
        if (str.length() < 3)
            return false;
        if ((str.startsWith("bad")) || (str.length() > 3 && str.startsWith("bad", 1)))
            return true;
        return false;
    }

    public static String conCat(String a, String b) {
        /* Given two strings, append them together (known as "concatenation") and return the result.
        However, if the concatenation creates a double-char, then omit one of the chars, so "abc" and "cat" yields "abcat".
        conCat("abc", "cat") → "abcat"
        conCat("dog", "cat") → "dogcat"
        conCat("abc", "") → "abc"
        */
        if (a.equals(""))
            return b;
        else if (b.equals(""))
            return a;
        if ((a.substring(a.length() - 1).equals(b.substring(0, 1))))
            return a.substring(0, a.length() - 1).concat(b);
        return a + b;
    }

    public static boolean frontAgain(String str) {
        /* Given a string, return true if the first 2 chars in the string also appear at the end of the string, such as with "edited".
        frontAgain("edited") → true
        frontAgain("edit") → false
        frontAgain("ed") → true
        */
        if (str.length() < 2)
            return false;
        return str.substring(0, 2).equals(str.substring(str.length() - 2)) ? true : false;
    }

    public static String without2(String str) {
        /*Given a string, if a length 2 substring appears at both its beginning and end, return a string without
        the substring at the beginning, so "HelloHe" yields "lloHe". The substring may overlap with itself, so "Hi" yields "".
        Otherwise, return the original string unchanged.
        without2("HelloHe") → "lloHe"
        without2("HelloHi") → "HelloHi"
        without2("Hi") → ""
        */
        if (str.length() >= 2 && str.substring(0, 2).equals(str.substring(str.length() - 2)))
            return str.substring(2);
        else
            return str;
    }

    public static String withoutX(String str) {
    /* Given a string, if the first or last chars are 'x', return the string without those 'x' chars, and otherwise return the string unchanged.
    withoutX("xHix") → "Hi"
    withoutX("xHi") → "Hi"
    withoutX("Hxix") → "Hxi"
    */
        if(str.length() > 0 && str.charAt(0) == 'x')
            str = str.substring(1);

        if(str.length() > 0 && str.charAt(str.length() - 1) == 'x')
            str = str.substring(0, str.length() - 1);

        return str;
    }

    public static String makeTags(String tag, String word) {
        /* The web is built with HTML strings like "<i>Yay</i>" which draws Yay as italic text.
        In this example, the "i" tag makes <i> and </i> which surround the word "Yay".
        Given tag and word strings, create the HTML string with tags around the word, e.g. "<i>Yay</i>".
        makeTags("i", "Yay") → "<i>Yay</i>"
        makeTags("i", "Hello") → "<i>Hello</i>"
        makeTags("cite", "Yay") → "<cite>Yay</cite>"
        */
        return "\"" + "<" + tag + ">" + word + "</" + tag + ">" + "\"";
    }

    public static String firstTwo(String str) {
    /* Given a string, return the string made of its first two chars, so the String "Hello" yields "He". If the string is shorter than length 2, return whatever there is, so "X" yields "X", and the empty string "" yields the empty string "". Note that str.length() returns the length of a string.
    firstTwo("Hello") → "He"
    firstTwo("abcdefg") → "ab"
    firstTwo("ab") → "ab"
    */
        if(str.length() < 2)
            return str;
        return str.substring(0, 2);
    }

    public static String comboString(String a, String b) {
    /* Given 2 strings, a and b, return a string of the form short+long+short, with the shorter string on the outside
    and the longer string on the inside. The strings will not be the same length, but they may be empty (length 0).
    comboString("Hello", "hi") → "hiHellohi"
    comboString("hi", "Hello") → "hiHellohi"
    comboString("aaa", "b") → "baaab"
    */
        if(a.length() > b.length())
            return b+a+b;
        return a+b+a;
    }

    public static String right2(String str) {
        /* Given a string, return a "rotated right 2" version where the last 2 chars are moved to the start. The string length will be at least 2.
        right2("Hello") → "loHel"
        right2("java") → "vaja"
        right2("Hi") → "Hi"
        */
        return str.substring(str.length()- 2) + str.substring(0, str.length()- 2);

    }

    public static String middleTwo(String str) {
    /* Given a string of even length, return a string made of the middle two chars, so the string "string" yields "ri". The string length will be at least 2.
    middleTwo("string") → "ri"
    middleTwo("code") → "od"
    middleTwo("Practice") → "ct"
    */
        return str.substring(str.length() / 2 - 1, str.length() / 2 + 1);
    }

    public static String twoChar(String str, int index) {
        /* Given a string and an index, return a string length 2 starting at the given index. If the index is too big
        or too small to define a string length 2, use the first 2 chars. The string length will be at least 2.
        twoChar("java", 0) → "ja"
        twoChar("java", 2) → "va"
        twoChar("java", 3) → "ja"
        */
        if(index < 0 || index > str.length() - 2)
            return str.substring(0, 2);
        return str.substring(index, index + 2);
    }

    public static String atFirst(String str) {
    /* Given a string, return a string length 2 made of its first 2 chars. If the string length is less than 2, use '@' for the missing chars.
    atFirst("hello") → "he"
    atFirst("hi") → "hi"
    atFirst("h") → "h@"
    */
        if(str.length() >=2)
            return str.substring(0, 2);
        if(str.length() == 1)
            return str + '@';
        return "@@";
    }

    public static String lastTwo(String str) {
    /* Given a string of any length, return a new string where the last 2 chars, if present, are swapped, so "coding" yields "codign".
    lastTwo("coding") → "codign"
    lastTwo("cat") → "cta"
    lastTwo("ab") → "ba"
    */
        if(str.length() < 2)
            return str;
        return str.substring(0, str.length()-2)  + str.substring(str.length()-1, str.length()) + str.substring(str.length()-2, str.length()-1);
    }

    public static String minCat(String a, String b) {
    /* Given two strings, append them together (known as "concatenation") and return the result.
    However, if the strings are different lengths, omit chars from the longer string so it is the same length as the shorter string.
    So "Hello" and "Hi" yield "loHi". The strings may be any length.
    minCat("Hello", "Hi") → "loHi"
    minCat("Hello", "java") → "ellojava"
    minCat("java", "Hello") → "javaello"
    */
        if(a.length() > b.length()) {
            return a.substring(a.length() - b.length()) + b;
        }
        return a + b.substring(b.length() - a.length());
    }

    public static String deFront(String str) {
    /* Given a string, return a version without the first 2 chars.
    Except keep the first char if it is 'a' and keep the second char if it is 'b'.
    The string may be any length. Harder than it looks.
    deFront("Hello") → "llo"
    deFront("java") → "va"
    deFront("away") → "aay"
    https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html
    */
        StringBuilder string = new StringBuilder();
        if (str.length() > 0 && str.charAt(0) == 'a') {
            string.append('a');
        }
        if (str.length() > 1 && str.charAt(1) == 'b') {
            string.append('b');
        }
        if (str.length() > 2) {
            string.append(str.substring(2));
        }
        return string.toString();
    }

    public static String withoutX2(String str) {
    /* Given a string, if one or both of the first 2 chars is 'x', return the string without those 'x' chars,
    and otherwise return the string unchanged. This is a little harder than it looks.
    withoutX2("xHi") → "Hi"
    withoutX2("Hxi") → "Hi"
    withoutX2("Hi") → "Hi"
    https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html
    */
        StringBuilder string = new StringBuilder();

        if(str. length() > 0 && str.charAt(0) != 'x')
            string.append(str.charAt(0));

        if(str. length() > 1 && str.charAt(1) != 'x')
            string.append(str.charAt(1));

        if(str.length()>2)
            string.append(str.substring(2));

        return string.toString();
    }

}