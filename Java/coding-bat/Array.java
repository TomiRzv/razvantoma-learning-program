import java.util.Arrays;

public class Array {
    public static void main(String[] args) {

     /* int[] nums = {1, 2, 6};
        boolean result = firstLast6(nums);
        System.out.println(result); */

     /*   int[] a = {1, 2, 3};
        int[] b = {7, 3};
        boolean result = commonEnd(a, b);
        System.out.println(result); */

      /*  int[] nums = {1, 2, 3};
        int[] reversedArray = reverse3(nums);
        System.out.println(Arrays.toString(reversedArray)); */
    }

    public static boolean firstLast6(int[] nums) {
    /* Given an array of ints, return true if 6 appears as either the first or last element in the array. The array will be length 1 or more.
    firstLast6([1, 2, 6]) → true
    firstLast6([6, 1, 2, 3]) → true
    firstLast6([13, 6, 1, 2, 3]) → false
    */
        return nums[0] == 6 || nums[nums.length - 1] == 6;
    }

    public static boolean commonEnd(int[] a, int[] b) {
    /* Given 2 arrays of ints, a and b, return true if they have the same first element or they have the same last element.
    Both arrays will be length 1 or more.
    commonEnd([1, 2, 3], [7, 3]) → true
    commonEnd([1, 2, 3], [7, 3, 2]) → false
    commonEnd([1, 2, 3], [1, 3]) → true
    */
        return a[0] == b[0] || a[a.length - 1] == b[b.length - 1];
    }

    public static int[] reverse3(int[] nums) {
    /* Given an array of ints length 3, return a new array with the elements in reverse order, so {1, 2, 3} becomes {3, 2, 1}.
    reverse3([1, 2, 3]) → [3, 2, 1]
    reverse3([5, 11, 9]) → [9, 11, 5]
    reverse3([7, 0, 0]) → [0, 0, 7]
    */
        int[] reversed = new int[3];
        for (int i = 0; i < 3; i++) {
            reversed[i] = nums[2 - i];
        }
        return reversed;
    }

}
