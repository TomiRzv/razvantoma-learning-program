package org.example;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.restassured.specification.QueryableRequestSpecification;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.SpecificationQuerier;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class AutomateRequestSpecification {

   // RequestSpecification requestSpecification;

    @BeforeClass
    public void beforeClass(){

    /*    requestSpecification = given().
                baseUri("https://api.postman.com").
                header("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544").
                log().all();  */
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://api.postman.com");
        requestSpecBuilder.addHeader("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544");
        requestSpecBuilder.log(LogDetail.ALL);

        RestAssured.requestSpecification = requestSpecBuilder.build();
    }

    @Test
    public void queryTest(){
        QueryableRequestSpecification queryableRequestSpecification = SpecificationQuerier.
                query(RestAssured.requestSpecification);

        System.out.println(queryableRequestSpecification.getBaseUri());
        System.out.println(queryableRequestSpecification.getHeaders());
    }

    @Test
    public void validate_status_code() {

      /*  RequestSpecification requestSpecification = given().
                baseUri("https://api.postman.com").
                header("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544"); */

      //  given(requestSpecification).
      /*  when().
                get("/workspaces"). */
        Response response = /*given(requestSpecification).
                header("dummyHeader", "dummyValue").*/
                get("/workspaces").then().log().all().extract().response();
        assertThat(response.statusCode(), is(equalTo(200)));

       /* then().
                log().all().
                assertThat().
                statusCode(200); */
    }

    @Test
    public void validate_response_body() {

      /*  given().
                baseUri("https://api.postman.com").
                header("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544"). */
       // given(requestSpecification).

        Response response = /*given(requestSpecification).*/ get("/workspaces").then().log().all().extract().response();
        assertThat(response.statusCode(), is(equalTo(200)));
        assertThat(response.path("workspaces[0].name").toString(), equalTo("My Workspace"));

       /* when().
                get("/workspaces").
        then().
                log().all().
                assertThat().
                statusCode(200).
                body("workspaces[0].name", equalTo("My Workspace")); */
    }
}