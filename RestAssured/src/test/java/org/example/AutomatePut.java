package org.example;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class AutomatePut {

    @BeforeClass
    public void beforeClass(){
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder().
                setBaseUri("https://api.postman.com").
                addHeader("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544").
                setContentType(ContentType.JSON).
                log(LogDetail.ALL);
        RestAssured.requestSpecification = requestSpecBuilder.build();

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder().
                expectStatusCode(200).
                expectContentType(ContentType.JSON).
                log(LogDetail.ALL);
        RestAssured.responseSpecification = responseSpecBuilder.build();
    }

    @Test
    public void validate_put_request_bdd_style(){

        String workspaceId = "940396c9-e4db-403a-87f5-11b5543e5bd7";
        String payload = "{\n" +
                "    \"workspace\": {\n" +
                "        \"name\": \"testNewWorkspace\",\n" +
                "        \"type\": \"personal\",\n" +
                "        \"description\": \"This is created by RestAssured\"\n" +
                "    }\n" +
                "}";

        given().
                body(payload).
                when().
                put("/workspaces/940396c9-e4db-403a-87f5-11b5543e5bd7").
                then().
                assertThat().
                body("workspace.name", equalTo("testNewWorkspace"),
                        "workspace.id", matchesPattern("^[a-z0-9-]{36}$"),
                        "workspace.id", equalTo(workspaceId));
    }
}
