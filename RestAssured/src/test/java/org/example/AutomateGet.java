package org.example;

import org.testng.annotations.Test;

import java.util.Collections;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class AutomateGet {

    @Test
    public void validate_get_status_code() {
       // Response res =
          // String name =
        given().
                baseUri("https://api.postman.com").
                header("X-Api-Key", "PMAK-64ddd73730d4e203a02166f0-0243569f89472e428eae7dd134944a9544").
        when().
                get("/workspaces").
        then().
                //log().all().
                assertThat().
                statusCode(200).
               /* extract().
                response().path("workspaces[0].name");
                body("workspaces.name", hasItems("My Workspace"),"workspaces.type", hasItems("personal")); */

                body("workspaces.name", contains("My Workspace"),
                        "workspaces.name", is(not(emptyArray())),
                        "workspaces.name", hasSize(1),
                        "workspaces[0]", hasKey("id"),
                        "workspaces[0]", hasValue("My Workspace"),
                        "workspaces[0]", hasEntry("id", "492362b7-937e-49b7-892b-85feeeb0cd39"),
                        "workspaces[0]", not(equalTo(Collections.EMPTY_MAP)),
                        "workspaces[0].name", allOf(startsWith("My"), containsString("Workspace"))
                );

        // System.out.println("Response = " + res.asString());
       // System.out.println("Workspace name = " + name);

        //Hamcrest//
        //assertThat(name, equalTo("My Workspace"));
        //TestNG//
       // Assert.assertEquals(name, "My Workspace");
    }
}