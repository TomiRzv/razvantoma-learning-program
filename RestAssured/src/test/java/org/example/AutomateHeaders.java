package org.example;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

import static io.restassured.RestAssured.*;

public class AutomateHeaders {

    @Test
    public void multiple_headers() {
        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                header("headerName", "value1").
                header("x-mock-match-request-headers", "headerName").
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void multiple_headers_using_Header_class() {

        Header header = new Header("headerName", "value1");
        Header matchHeader = new Header("x-mock-match-request-headers", "headerName");

        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                header(header).
                header(matchHeader).
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void multiple_headers_using_Headers_class() {

        Header header = new Header("headerName", "value1");
        Header matchHeader = new Header("x-mock-match-request-headers", "headerName");

        Headers headers = new Headers(header, matchHeader);

        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                headers(headers).
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void multiple_headers_using_Map_collection() {

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("headerName", "value1");
        headers.put("x-mock-match-request-headers", "headerName");

        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                headers(headers).
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void multi_value_header_in_the_request() {

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("headerName", "value1");
        headers.put("x-mock-match-request-headers", "headerName");


        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                header("multiValueHeader", "value1", "value2").
                log().headers().
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void multi_value_header_in_the_request_using_Header() {
    // HashMap nu poate fi folosit fiindca nu poate contine key duplicate
        Header header1  = new Header("multiValueHeader", "value1");
        Header header2 = new Header("multiValueHeader", "value2");

        Headers headers = new Headers(header1, header2);

        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                headers(headers).
                log().headers().
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200);
    }

    @Test
    public void assert_response_headers(){

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("headerName", "value1");
        headers.put("x-mock-match-request-headers", "headerName");

        given().
                baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                headers(headers).
        when().
                get("/get").
        then().
                log().all().
                assertThat().
                statusCode(200).
                headers("responseHeader", "resValue1",
                        "X-RateLimit-Limit", "120");
               // header("responseHeader", "resValue1").
               // header("X-RateLimit-Limit", "120");
    }

    @Test
    public void extract_response_headers() {

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("headerName", "value1");
        headers.put("x-mock-match-request-headers", "headerName");

        Headers extractHeaders =
                given().
                        baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                        headers(headers).
                        when().
                        get("/get").
                        then().
                        assertThat().
                        statusCode(200).
                        extract().
                        headers();

        for (Header header : extractHeaders) {
            System.out.println("header name = " + header.getName() + ", ");
            System.out.println("header value = " + header.getValue());
            System.out.println();
        }
    }

        /*System.out.println("header name = " +  extractHeaders.get("responseHeader").getName());
        System.out.println("header value = " +  extractHeaders.get("responseHeader").getValue());
        System.out.println("header value = " +  extractHeaders.getValue("responseHeader"));*/

    @Test
    public void extract_multi_value_response_headers () {

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("headerName", "value1");
        headers.put("x-mock-match-request-headers", "headerName");

        Headers extractedHeaders =
                given().
                        baseUri("https://1a5acc97-13c2-4fcc-8273-1b3452600167.mock.pstmn.io").
                        headers(headers).
                when().
                        get("/get").
                then().
                        assertThat().
                        statusCode(200).
                        extract().
                        headers();

        List<String> values = extractedHeaders.getValues("multiValueHeader");
        for(String value:values){
            System.out.println(value);
        }
    }
}